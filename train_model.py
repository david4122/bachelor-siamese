from utils import make_pairs, plt_metric, plt_pairs, save_model
from config import *


(x_train, y_train), (x_test, y_test) = dataset.load_data()
# x_train, x_test = x_train / 255, x_test / 255 # CAUSES HIGH MEMORY USAGE
# x_train, y_train = x_train[:100], y_train[:100]
pairs_train, labels_train = make_pairs(x_train, y_train, num_classes)
pairs_test, labels_test = make_pairs(x_train, y_train, num_classes)

del x_train, y_train, x_test, y_test

model = build_model()
model.summary(expand_nested=True)

class LogLearningRate(tf.keras.callbacks.Callback):

    def on_epoch_begin(self, epoch, logs=None):
        print('Training rate for epoch {}: {}'.format(epoch, self.model.optimizer.lr))

history = model.fit(
        pairs_train,
        labels_train,
        validation_data=(pairs_test, labels_test),
        epochs=train_epochs,
        batch_size=batch_size,
        shuffle=True,
        callbacks=[LogLearningRate()])

loss_fig = plt_metric(history.history, 'loss', 'Loss')
acc_fig = plt_metric(history.history, 'accuracy', 'Accuracy')

x_pred_1, x_pred_2 = pairs_test
pairs_pred = [x_pred_1[:16], x_pred_2[:16]]
preds = model.predict(pairs_pred)
plt_pairs(pairs_pred, labels_test, 16, 4, preds)

while True:
    save_current_model = input('Save model? [y/n] ').lower()

    if save_current_model == 'y':
        scores_train = model.evaluate(pairs_train, labels_train)
        scores_test = model.evaluate(pairs_test, labels_test)
        save_model(models_dir, hparams['model_name'], model, scores_train, scores_test, loss_fig, acc_fig, dataset.__name__)
        break

    elif save_current_model == 'n':
        break

    print('Enter either `y` or `n`')
