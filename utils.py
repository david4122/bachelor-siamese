import tensorflow as tf
import json
import numpy as np
from numpy import ndarray
from pathlib import Path
import matplotlib.pyplot as plt


def euclidean_distance(vects):
    x, y = vects
    sum_square = tf.math.reduce_sum(tf.math.square(x - y), axis=1, keepdims=True)
    return tf.math.sqrt(tf.math.maximum(sum_square, tf.keras.backend.epsilon()))


def plt_pairs(pairs, labels, to_show=6, num_col=3, predictions=None, dtype='int'):
    num_row = int(np.ceil(to_show / num_col))

    # Plot the images
    fig, axes = plt.subplots(num_row, num_col, figsize=(15, 10))
    for i in range(to_show):
        ax = axes[i // num_col][i % num_col]

        ax.imshow(tf.concat([pairs[0][i].astype(dtype), pairs[1][i].astype(dtype)], axis=1), cmap="gray")
        ax.set_axis_off()
        if predictions is None:
            ax.set_title("Label: {}".format(labels[i]))
        else:
            ax.set_title("True: {} | Pred: {:.5f}".format(labels[i], predictions[i][0]))

    plt.show()


def make_pairs(x: ndarray, y: ndarray, num_classes: int) -> tuple[tuple[ndarray, ndarray], ndarray]:
    by_label = [(y == i).nonzero()[0] for i in range(num_classes)]

    firsts = []
    seconds = []
    labels = []

    for idx1 in range(len(x)):
        cur_y = y[idx1][0]

        # positive sample
        idx2 = np.random.choice(by_label[cur_y])

        firsts.append(x[idx1])
        seconds.append(x[idx2])
        labels.append(0)

        # negative sample
        neg_y = np.random.choice([ i for i in range(len(by_label)) if i != cur_y ])
        idx2 = np.random.choice(by_label[neg_y])

        firsts.append(x[idx1])
        seconds.append(x[idx2])
        labels.append(1)

    return (np.array(firsts), np.array(seconds)), np.array(labels)


def plt_metrics(title, metric, histories, n_cols=3):
    n_rows = int(np.ceil(len(histories) / n_cols))
    fig, axs = plt.subplots(n_rows, n_cols)
    fig.suptitle(title)

    i = 0
    for history in histories:
        ax = axs[i // n_cols][i % n_cols]

        ax.plot(history[metric])
        if history.get(f'val_{metric}') is not None:
            ax.plot(history.get(f'val_{metric}'))

        ax.legend(["train", "validation"], loc="upper left")
        ax.set_title(f'run {i}')

        i += 1

    plt.show()


def plt_metric(history, metric, title, show=True):
    fig, ax = plt.subplots()
    ax.plot(history[metric])

    if history.get(f'val_{metric}') is not None:
        ax.plot(history.get(f'val_{metric}'))
        ax.legend(["train", "validation"], loc="upper left")

    ax.set_title(title)
    ax.set_ylabel(metric)
    ax.set_xlabel("epoch")

    if show:
        plt.show()

    return fig


def save_model(
        models_dir: str,
        model_name: str,
        model: tf.keras.Model,
        train_scores: list[float],
        test_scores: list[float],
        loss_fig,
        acc_fig,
        dataset: str,
) -> None:
    path = Path(models_dir, model_name)
    if not path.exists():
        path.mkdir(parents=True)

    model.save(path / 'model.keras')
    try:
        utils.plot_model(
                model,
                path / 'model.svg',
                dpi=None,
                show_shapes=True,
                show_layer_activations=True,
                show_trainable=True,
                expand_nested=True)
    except:
        print('Plot model crashed')

    loss_fig.savefig(path / 'loss.svg')
    acc_fig.savefig(path / 'accuracy.svg')

    metadata = {
            'name': model_name,
            'train_loss': np.round(train_scores[0], 2),
            'train_accuracy': np.round(train_scores[1], 2),
            'test_loss': np.round(test_scores[0], 2),
            'test_accuracy': np.round(test_scores[1], 2),
            'train_dataset': dataset,
        }

    with (path / 'metadata.json').open('w') as f:
        json.dump(metadata, f)
