from keras import layers, optimizers, applications, Model

def optimizer():
    lr_schedule = optimizers.schedules.ExponentialDecay(
            initial_learning_rate=0.01,
            decay_steps=10000,
            decay_rate=0.9)
    return optimizers.Adam(lr_schedule)


resnet50_hparams = {
        'model_name': 'resnet50',
        'loss': 'binary_crossentropy',
        'optimizer': optimizer,

        'sister_net': {
            'dense_units': [ 4096, 1024, 128 ],
            'dropout': 0.15
        },
    }


def resnet50_net(hparams, input_shape, num_classes):
    base_model = applications.ResNet50(
            include_top=False,
            weights='imagenet',  # Load weights pre-trained on ImageNet.
            input_shape=input_shape,
            classes=num_classes,
            pooling='max')
    base_model.trainable = False

    inp = layers.Input(input_shape)
    x = layers.Lambda(applications.resnet.preprocess_input)(inp)
    x = base_model(x)
    x = layers.BatchNormalization()(x)

    for units in hparams['dense_units']:
        x = layers.Dropout(hparams['dropout'])(x)
        x = layers.Dense(units, activation='relu')(x)

    return Model(inp, x)
