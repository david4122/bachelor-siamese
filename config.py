import tensorflow as tf
from pathlib import Path
from siamese_model import build_siamese_model
from default_model import *
from resnet50_model import resnet50_hparams, resnet50_net
from vgg16_model import vgg16_hparams, vgg16_net

models_dir = 'models'
input_shape = (32, 32, 3)
batch_size = 32
dataset = tf.keras.datasets.cifar10
num_classes = 10

hparams = default_hparams
build_sister_net = default_sister_net
get_hparams_with_hp = get_default_hparams_with_hp

# hparams = resnet50_hparams
# build_sister_net = resnet50_net

# hparams = vgg16_hparams
# build_sister_net = vgg16_net


def build_model():
    return build_siamese_model(hparams, build_sister_net, input_shape, num_classes)


def build_model_with_hp(hp):
    return build_siamese_model(get_hparams_with_hp(hp), build_sister_net, input_shape, num_classes)


cv_epochs = 5
cv_n_split = 5

# train_epochs = 50
train_epochs = 5

tuner_epochs = 10
tuner_max_trials = 10
