import tensorflow as tf
import numpy as np
from sklearn.model_selection import KFold
from utils import make_pairs, plt_pairs, plt_metrics
from siamese_model import build_siamese_model
from config import *


def cross_validate(build_model, pairs_train_val, labels_train_val):
    kfold = KFold(n_splits=cv_n_split, shuffle=True)
    fold_no = 1
    records = []

    x_1, x_2 = pairs_train_val
    for train, val in kfold.split(pairs_train_val[0], labels_train_val):
        try:
            model = build_model()

            print('------------------------------------------------------------------------')
            print(f'Running fold {fold_no} / {cv_n_split} ...')

            history = model.fit(
                [x_1[train], x_2[train]],
                labels_train_val[train],
                validation_data=([x_1[val], x_2[val]], labels_train_val[val]),
                epochs=cv_epochs,
                batch_size=batch_size)

            records.append(history.history)

        except KeyboardInterrupt:
            print('Skipping to next fold...')

        fold_no += 1

    plt_metrics('Loss', 'loss', records)
    plt_metrics('Accuracy', 'accuracy', records)


(x_train_val, y_train_val), (x_test, y_test) = dataset.load_data()
# x_train_val, y_train_val = x_train_val[:100], y_train_val[:100]

# data_augmentation = tf.keras.Sequential([
#     layers.RandomFlip("horizontal_and_vertical"),
#     layers.RandomRotation(0.2),
# ])

# x_train = data_augmentation(x_train)

# x_train_val, x_test = x_train_val / 255, x_test / 255 # CAUSES HIGH MEMORY USAGE
pairs_train_val, labels_train_val = make_pairs(x_train_val, y_train_val, num_classes)
# pairs_test, labels_test = make_pairs(x_test, y_test, num_classes)

cross_validate(build_model, pairs_train_val, labels_train_val)
# cross_validate(lambda : build_siamese_model(default_hparams, default_sister_net), pairs_train_val, labels_train_val)