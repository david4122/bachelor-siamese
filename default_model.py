import tensorflow as tf


def optimizer():
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=0.0001,
            decay_steps=10000,
            decay_rate=0.9)
    return tf.keras.optimizers.Adam(lr_schedule)


default_hparams = {
    'model_name': 'default',
    'loss': 'binary_crossentropy',
    'optimizer': optimizer,

    'sister_net': {
        'conv_blocks': [
            [ 64 ],
            [ 128 ],
            [ 256 ],
            [ 512, 1024 ],
        ],
        'dropout': 0.15,
        'dense_units': [ 4096, 1024, 128 ],
    }
}


def get_default_hparams_with_hp(hp):
    return {
        'model_name': 'default',
        'loss': 'binary_crossentropy',
        'optimizer': optimizer,

        'sister_net': {
            'conv_blocks': [
                [ hp.Choice('conv_1', [32, 64]) ],
                [ hp.Choice('conv_2', [64, 128]) ],
                [ hp.Choice('conv_3', [128, 256]) ],
                [ hp.Choice('conv_4_1', [1024, 512]), hp.Choice('conv_4_2', [512, 256]) ],
            ],
            'dropout': 0.15,
            'dense_units': [ 4096, 1024, 128 ],
        }
    }


def default_sister_net(hparams, input_shape: tuple[int], num_classes: int):
    input = tf.keras.layers.Input(input_shape)

    x = tf.keras.layers.Lambda(tf.keras.applications.imagenet_utils.preprocess_input)(input)

    for filters in hparams['conv_blocks']:
        x = conv_pool_block(filters, hparams['dropout'])(x)

    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.BatchNormalization()(x)

    for units in hparams['dense_units']:
        x = tf.keras.layers.Dense(units, activation='relu')(x)
        x = tf.keras.layers.Dropout(hparams['dropout'])(x)

    return tf.keras.Model(input, x)


def conv_pool_block(filters, dropout=0.1):
    def block(inp):
        x = tf.keras.layers.BatchNormalization()(inp)
        for f in filters:
            x = tf.keras.layers.SpatialDropout2D(dropout)(x)
            x = tf.keras.layers.Conv2D(f, kernel_size=(3, 3), padding='same', activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.00001))(x)
            # x = tf.keras.layers.Conv2D(f, kernel_size=(3, 3), padding='same', activation='relu')(x)
        return tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(x)

    return block
