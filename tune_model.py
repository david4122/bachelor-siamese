from keras import layers, optimizers, applications, Model
from sklearn.model_selection import train_test_split
import keras_tuner
from utils import make_pairs, plt_pairs, plt_metric
from config import *


(x_train, y_train), (x_test, y_test) = dataset.load_data()
# x_train, y_train = x_train[:100], y_train[:100]

pairs_train, labels_train = make_pairs(x_train, y_train, num_classes)
pairs_test, labels_test = make_pairs(x_test, y_test, num_classes)

plt_pairs(pairs_train, labels_train, num_col=6, to_show=36)


tuner = keras_tuner.RandomSearch(
        build_model_with_hp,
        objective='val_accuracy',
        max_trials=10,
        directory='tuner_save_state',
        project_name=hparams['model_name'])

tuner.search(
        pairs_train,
        labels_train,
        epochs=tuner_epochs,
        batch_size=batch_size,
        validation_data=(pairs_test, labels_test))


best_params = tuner.get_best_hyperparameters()[0].values
print('BEST MODEL FOUND:', best_params)