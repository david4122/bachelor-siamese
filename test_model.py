from keras import  models, datasets
from os import getenv
from sys import argv
from utils import make_pairs, plt_pairs, plt_metric
from config import models_dir, hparams, num_classes


model_path = argv[1] if len(argv) > 1 else f'{models_dir}/{hparams["model_name"]}/model.keras'

print('### Loading model from {}'.format(model_path))
model = models.load_model(model_path, safe_mode=False)
model.summary()

(x_train, y_train), (x_test, y_test) = datasets.cifar100.load_data()

pairs_train, labels_train = make_pairs(x_train, y_train, num_classes)
pairs_test, labels_test = make_pairs(x_test, y_test, num_classes)

res = model.evaluate(pairs_train, labels_train)
print("TRAIN: [loss, acc]:", res)

res = model.evaluate(pairs_test, labels_test)
print("TEST: [loss, acc]:", res)

show_n = 36
predictions = model.predict([pairs_test[0][:show_n], pairs_test[1][:show_n]])
plt_pairs(pairs_train, labels_train, num_col=6, to_show=show_n, predictions=predictions)

