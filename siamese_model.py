from keras import layers, Model
from utils import euclidean_distance


def build_siamese_model(hparams, build_sister_net, input_shape: tuple[int], num_classes: int):
    net = build_sister_net(hparams['sister_net'], input_shape, num_classes)

    input_1 = layers.Input(input_shape)
    input_2 = layers.Input(input_shape)

    tower_1 = net(input_1)
    tower_2 = net(input_2)

    merge_layer = layers.Lambda(euclidean_distance)([tower_1, tower_2])
    normal_layer = layers.BatchNormalization()(merge_layer)
    output_layer = layers.Dense(1, activation="sigmoid")(normal_layer)

    siamese = Model(name='siamese', inputs=[input_1, input_2], outputs=output_layer)

    siamese.compile(loss=hparams['loss'], optimizer=hparams['optimizer'](), metrics=["accuracy"])

    return siamese
